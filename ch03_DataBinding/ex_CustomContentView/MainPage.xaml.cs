﻿using System.Windows.Input;

namespace ex_CustomContentView;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
		InitCommands();
		BindingContext = this;
	}

    public Artist Artist1 { get; set; } 
		= new Artist 
		{ 
			Name = "Delphine Joussein",
			Instrument = "flute, fx, voice",
			Picture = "joussein.png"
		}; 

	public Artist Artist2 { get; set; } 
		= new Artist 
		{ 
			Name = "Rafaëlle Rinaudo",
			Instrument = "electric harp",
			Picture = "rinaudo.png"
		}; 

	public Artist Artist3 { get; set; } 
		= new Artist 
		{ 
			Name = "Blanche Lafuente",
			Instrument = "drums, drum pad",
			Picture = "lafuente.png"
		}; 

	public Color MainColor { get; set; } = Colors.DarkSalmon;

	void OnNextClicked(object sender, EventArgs e)
	{
		DisplayAlert("Next", $"You have clicked on Next of an {sender.GetType().Name}", "Ok");
	}

	void OnInfoClicked(object sender, EventArgs e)
	{
		DisplayAlert("Info", $"You have clicked on Information of a {sender.GetType().Name}, whose Title is {(sender as MyAvatarView)?.Title}", "Ok");
	}

	int colorIndex=0;
	Color[] colors = { Colors.DarkKhaki, Colors.DarkGoldenrod, Colors.DarkOliveGreen, Colors.DarkOrchid, Colors.DarkSeaGreen, Colors.DarkTurquoise, Colors.DarkViolet, Colors.DarkSalmon };
	private void OnTapped(object sender, TappedEventArgs e)
	{
		if(sender is not MyAvatarView) return;
		(sender as MyAvatarView)!.Color = colors[colorIndex];
		colorIndex++;
		colorIndex %= colors.Length;
	}

	public ICommand MyNextCommand { get; set; }
	public ICommand MyInfoCommand { get; set; }
	public ICommand MyTapCommand { get; set; }

	private void InitCommands()
    {
        MyNextCommand = new Command<object>(
			str => DisplayAlert("Next2", $"This view is related to {str}", "Ok")
		);

		MyInfoCommand = new Command<Artist>(
			artist => DisplayAlert("Info2", $"This view is related to {artist.Name}", "Ok")
		);

		MyTapCommand = new Command<MyAvatarView>(
			av => DisplayAlert("Tap", $"This view is related to {av.Title}", "Ok")
		);
    }
}

