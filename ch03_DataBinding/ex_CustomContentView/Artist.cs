﻿namespace ex_CustomContentView;

public class Artist
{
    public string Name { get; set; }
    public string Instrument { get; set; }
    public string Picture { get; set; }
}
