using System.Windows.Input;

namespace ex_CustomContentView;

public partial class MyAvatarView : ContentView
{
	public MyAvatarView()
	{
		InitializeComponent();
	}

	public static readonly BindableProperty ImageNameProperty =
		BindableProperty.Create("ImageName", typeof(string), typeof(MyAvatarView), "");
	
	public string ImageName
	{
		get => (string)GetValue(ImageNameProperty);
		set => SetValue(ImageNameProperty, value);
	}

	public static readonly BindableProperty TitleProperty =
		BindableProperty.Create("Title", typeof(string), typeof(MyAvatarView), "No Title");
	
	public string Title
	{
		get => (string)GetValue(TitleProperty);
		set => SetValue(TitleProperty, value);
	}

	public static readonly BindableProperty SubTitleProperty =
		BindableProperty.Create("SubTitle", typeof(string), typeof(MyAvatarView), "Lorem Ipsum Dolor");
	
	public string SubTitle
	{
		get => (string)GetValue(SubTitleProperty);
		set => SetValue(SubTitleProperty, value);
	}

	public static readonly BindableProperty ColorProperty =
		BindableProperty.Create("Color", typeof(Color), typeof(MyAvatarView), Colors.Gray);
	
	public Color Color
	{
		get => (Color)GetValue(ColorProperty);
		set => SetValue(ColorProperty, value);
	}

	public event EventHandler NextClicked
	{
		add => nextButton.Clicked += value;
		remove => nextButton.Clicked -= value;
	}	

	public event EventHandler InfoClicked;

	void OnInfoClicked(object sender, EventArgs args)
	{
		InfoClicked?.Invoke(this, args);
	}

	public event EventHandler<TappedEventArgs> Tapped;

	private void OnTapped(object sender, TappedEventArgs e)
	{
		Tapped?.Invoke(this, e);
	}

	public static readonly BindableProperty NextCommandProperty =
		BindableProperty.Create("NextCommand", typeof(ICommand), typeof(MyAvatarView), null);
	
	public ICommand NextCommand
	{
		get => (ICommand)GetValue(NextCommandProperty);
		set => SetValue(NextCommandProperty, value);
	}

	public static readonly BindableProperty NextCommandParameterProperty =
		BindableProperty.Create("NextCommandParameter", typeof(object), typeof(MyAvatarView), null);
	
	public object NextCommandParameter
	{
		get => GetValue(NextCommandParameterProperty);
		set => SetValue(NextCommandParameterProperty, value);
	}

	public static readonly BindableProperty InfoCommandProperty =
		BindableProperty.Create("InfoCommand", typeof(ICommand), typeof(MyAvatarView), null);
	
	public ICommand InfoCommand
	{
		get => (ICommand)GetValue(InfoCommandProperty);
		set => SetValue(InfoCommandProperty, value);
	}

	public static readonly BindableProperty InfoCommandParameterProperty =
		BindableProperty.Create("InfoCommandParameter", typeof(object), typeof(MyAvatarView), null);
	
	public object InfoCommandParameter
	{
		get => GetValue(InfoCommandParameterProperty);
		set => SetValue(InfoCommandParameterProperty, value);
	}

	public static readonly BindableProperty TapCommandProperty =
		BindableProperty.Create("TapCommand", typeof(ICommand), typeof(MyAvatarView), null);
	
	public ICommand TapCommand
	{
		get => (ICommand)GetValue(TapCommandProperty);
		set => SetValue(TapCommandProperty, value);
	}

	public static readonly BindableProperty TapCommandParameterProperty =
		BindableProperty.Create("TapCommandParameter", typeof(object), typeof(MyAvatarView), null);
	
	public object TapCommandParameter
	{
		get => GetValue(TapCommandParameterProperty);
		set => SetValue(TapCommandParameterProperty, value);
	}
}