﻿using ex_BindingToA2dArray_context;

namespace ex_BindingToA2dArray_v1;

public partial class MainPage : ContentPage
{
	public Matrix2d Matrix {get;} = new Matrix2d(7, 6);

	public MainPage()
	{
		InitializeComponent();
		BindingContext = this;
	}
}

