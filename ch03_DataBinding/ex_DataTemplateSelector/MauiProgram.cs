﻿using CommunityToolkit.Maui;
using Microsoft.Extensions.Logging;

namespace ex_DataTemplateSelector;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.UseMauiCommunityToolkit()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
				fonts.AddFont("SF-Pro-Text-Black.otf", "SF Pro Text Black");
				fonts.AddFont("SF-Pro-Text-Bold.otf", "SF Pro Text Bold");
				fonts.AddFont("SF-Pro-Text-Heavy.otf", "SF Pro Text Heavy");
				fonts.AddFont("SF-Pro-Text-Semibold.otf", "SF Pro Text Semibold");
			});

#if DEBUG
		builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}
