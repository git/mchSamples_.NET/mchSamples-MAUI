﻿using ex_DataTemplateSelector.Model;
namespace ex_DataTemplateSelector;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
		BindingContext = new SearchResults();
	}
}

