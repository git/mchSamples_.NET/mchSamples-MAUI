﻿using ex_DataTemplateSelector.Model;

namespace ex_DataTemplateSelector;

public class ResultSelector : DataTemplateSelector
{
    public DataTemplate? AlbumTemplate { get; set; }
    public DataTemplate? PlaylistTemplate { get; set; }
    public DataTemplate? TrackTemplate { get; set; }
    public DataTemplate? ArtistTemplate { get; set; }
    protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
    {
        return item switch
        {
            Playlist _ => PlaylistTemplate,
            Album _ => AlbumTemplate,
            Track _ => TrackTemplate,
            Artist _ => ArtistTemplate,
            _ => throw new ArgumentException()
        };
    }
}
