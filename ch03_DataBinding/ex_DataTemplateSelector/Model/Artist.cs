﻿namespace ex_DataTemplateSelector.Model;

public class Artist
{
    public string? FullName { get; set; }

    public string? Picture { get; set; }
}
