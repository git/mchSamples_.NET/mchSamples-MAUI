﻿namespace ex_DataTemplateSelector.Model;

public class SearchResults
{
    public IEnumerable<object> Results { get; set; }

    public SearchResults()
    {
        var johnColtrane = new Artist
            {
                FullName = "John Coltrane",
                Picture = "john_coltrane.png"
            };

        var playlistColtrane = new Playlist
            {
                Title = "John Coltrane - les indispensables",
                Owner = "Apple Music : Jazz",
                Image = "playlist_john_coltrane.jpg"
            };

        var aliceColtrane = new Artist
            {
                FullName = "Alice Coltrane",
                Picture = "alice_coltrane.png"
            };

        var myFavoriteThingsTrack = new Track
            {
                Title = "My Favorite Things",
                Album = new Album
                {
                    Title = "My Favorite Things",
                    Image = "my_favorite_things.png",
                    Artist = johnColtrane
                }
            };

        var coltraneQuartetLp = new Album
            {
                Title = "Coltrane",
                Image = "john_coltrane_quartet.jpg",
                Artist = johnColtrane
            };
        
        var coltraneLp = new Album  
            {
                Title = "Coltrane (Remastered)",
                Image = "coltrane_7105.jpg",
                Artist = johnColtrane
            };

        Results = new object[] { johnColtrane, playlistColtrane, aliceColtrane, myFavoriteThingsTrack, coltraneQuartetLp, coltraneLp };
    }
}
