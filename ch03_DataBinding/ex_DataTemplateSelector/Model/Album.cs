﻿namespace ex_DataTemplateSelector.Model;

public class Album
{
    public string? Title { get; set; }

    public Artist? Artist { get; set; }

    public string? Image { get; set; }
}
