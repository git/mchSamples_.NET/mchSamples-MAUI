﻿namespace ex_DataTemplateSelector.Model;

public class Track
{
    public string? Title { get; set; }

    public Album? Album { get; set; }
}
