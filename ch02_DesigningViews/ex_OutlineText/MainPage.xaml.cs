﻿using System.ComponentModel;
using SkiaSharp;
using SkiaSharp.Views.Maui;

namespace ex_OutlineText;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
		BindingContext = new SimpleVM();
	}
}

