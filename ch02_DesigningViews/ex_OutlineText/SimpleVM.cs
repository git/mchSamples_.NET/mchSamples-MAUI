﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ex_OutlineText;

public class SimpleVM : INotifyPropertyChanged
{
    public string Text
	{
		get => text;
		set
		{
			text = value;
			OnPropertyChanged();
		}
	}
	private string text = "Prout";

	public float StrokeWidth
	{
		get => strokeWidth;
		set
		{
			strokeWidth = value;
			OnPropertyChanged();
		}
	}
	private float strokeWidth;

	public string FontFamily
	{
		get => font;
		set
		{
			font = value;
			OnPropertyChanged();
		}
	}
	private string font = "Equestria";

    public void OnPropertyChanged([CallerMemberName] string pptyName = "")
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(pptyName));
    }

    public event PropertyChangedEventHandler? PropertyChanged;
}
