﻿namespace ex_ResponsivePage;

public sealed class IdiomStateTrigger : Microsoft.Maui.Controls.StateTriggerBase
{
    public static readonly BindableProperty IdiomProperty = 
        BindableProperty.Create("Idiom", typeof(string), typeof(IdiomStateTrigger), "phone");

    public string Idiom
    {
        get => GetValue(IdiomProperty) as string;
        set => SetValue(IdiomProperty, value);
    } 

    protected override void OnAttached()
    {
        base.OnAttached();

        UpdateState();
    }

    void UpdateState()
    {
        if (string.IsNullOrEmpty(Idiom))
            return;

        var idiom = DeviceIdiom.Create(Idiom);

        SetActive(DeviceInfo.Current.Idiom == idiom);
    }
}
