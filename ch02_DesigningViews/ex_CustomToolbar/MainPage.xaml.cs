﻿namespace ex_CustomToolbar;

public partial class MainPage : ContentPage
{
	int count = 0;

	public MainPage()
	{
		InitializeComponent();
	}

	private void OnCounterClicked(object sender, EventArgs e)
	{
		count++;

		if (count == 1)
			CounterBtn.Text = $"Clicked {count} time";
		else
			CounterBtn.Text = $"Clicked {count} times";

		SemanticScreenReader.Announce(CounterBtn.Text);
	}

	private void EllipsisClicked(object sender, EventArgs e)
	{
		secondaryMenu.IsVisible = !secondaryMenu.IsVisible;
	}

	private void BackgroundClicked(object sender, TappedEventArgs e)
	{
		secondaryMenu.IsVisible = false;
	}
}

