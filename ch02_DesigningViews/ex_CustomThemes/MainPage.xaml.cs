﻿namespace ex_CustomThemes;
using System.Linq;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();

		// Application.Current.RequestedThemeChanged += (s, a) =>
		// {
		// 	if(Preferences.Default.Get<string>("Theme", null) != "System")
		// 	{
		// 		return;
		// 	}
		// 	ResourceDictionary chosenTheme = Application.Current.RequestedTheme switch
		// 		{
		// 			AppTheme.Light => new LightTheme(),
		// 			AppTheme.Dark => new DarkTheme(),
		// 			_ => new LightTheme()
		// 		};

		// 	ICollection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
		// 	if (mergedDictionaries != null)
		// 	{
		// 		foreach(var dico in mergedDictionaries.Where(d => d is ICustomThemes).ToList())
		// 		{
		// 			mergedDictionaries.Remove(dico);
		// 		}
		// 		mergedDictionaries.Add(chosenTheme);
		// 	}
		// };

	}

	void picker_SelectedIndexChanged(object source, EventArgs args)
	{
		string? chosenThemeString = picker.SelectedItem as string;

		ResourceDictionary chosenTheme = chosenThemeString switch 
		{
			"Dark" => new DarkTheme(),
			"Light" => new LightTheme(),
			"Color Blind" => new ColorBlindTheme(),
			_ => new LightTheme()
		};

		ICollection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
		if (mergedDictionaries != null)
		{
			foreach(var dico in mergedDictionaries.Where(d => d is ICustomTheme).ToList())
			{
				mergedDictionaries.Remove(dico);
			}
			mergedDictionaries.Add(chosenTheme);
		}

		// ResourceDictionary chosenTheme;
		// switch(chosenThemeString) 
		// {
		// 	case "Dark":
		// 	 	chosenTheme = new DarkTheme();
		// 		Preferences.Default.Set("Theme", "Dark");
		// 		break;
		// 	case "Light":
		// 	 	chosenTheme = new LightTheme();
		// 		Preferences.Default.Set("Theme", "Light");
		// 		break;
		// 	case "Color Blind":
		// 	 	chosenTheme = new ColorBlindTheme();
		// 		Preferences.Default.Set("Theme", "Color Blind");
		// 		break;
		// 	default:
		// 		Preferences.Default.Set("Theme", "System");
		// 		chosenTheme = Application.Current.RequestedTheme switch
		// 		{
		// 			AppTheme.Light => new LightTheme(),
		// 			AppTheme.Dark => new DarkTheme(),
		// 			_ => new LightTheme()
		// 		};
		// 		break;
		// };

		// ICollection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
		// if (mergedDictionaries != null)
		// {
		// 	foreach(var dico in mergedDictionaries.Where(d => d is ICustomThemes).ToList())
		// 	{
		// 		mergedDictionaries.Remove(dico);
		// 	}
		// 	mergedDictionaries.Add(chosenTheme);
		// }
	}
}

