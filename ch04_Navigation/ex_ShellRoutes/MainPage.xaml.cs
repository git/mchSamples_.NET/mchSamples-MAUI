﻿namespace ex_ShellRoutes;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
	}

	private async void GoToPage1(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("page1");
	}

	private async void GoToAbsolutePage1(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("//page1");
	}

	private async void GoToPage2(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("page2");
	}

	private async void GoToPage1_2_2(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("page1/page1_2/page1_2_2");
	}

	private async void GoToAbsolutePage1_2_2(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("//page1/page1_2/page1_2_2");
	}
}

