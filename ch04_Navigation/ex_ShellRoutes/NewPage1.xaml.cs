namespace ex_ShellRoutes;

public partial class NewPage1 : ContentPage
{
	public NewPage1()
	{
		InitializeComponent();
		BindingContext = Shell.Current;
	}

	private async void GoToNewPage1(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("newpage1");
	}

	private async void GoToAbsoluteNewPage1(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("//newpage1");
	}

	private async void PushNewPage2(object sender, EventArgs e)
	{
		//await Shell.Current.GoToAsync("newpage2");
		await Navigation.PushAsync(new NewPage2());
	}

	private async void GoToNewPage4(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("newpage1/newpage4");
	}
}