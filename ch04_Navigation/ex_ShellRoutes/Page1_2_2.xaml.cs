namespace ex_ShellRoutes;

public partial class Page1_2_2 : ContentPage
{
	public Page1_2_2()
	{
		InitializeComponent();
	}

    protected override void OnNavigatedTo(NavigatedToEventArgs args)
    {
		labelNavStack.Text = Shell.Current.Navigation.NavigationStack.Aggregate("Stack", (total, next) => $"{total} > {next?.Title ?? ""}");
	}

	private async void GoToPage1_2_1(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("page1_2_1");
	}

	private async void GoBackThenToPage1_2_3(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("../page1_2_3");
	}

	private async void GoBack(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("..");
	}

	private async void GoBackTwiceThenToPage2(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("../../page2");
	}
}