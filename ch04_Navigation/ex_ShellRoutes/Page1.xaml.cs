using System.Text;

namespace ex_ShellRoutes;

public partial class Page1 : ContentPage
{
	public Page1()
	{
		InitializeComponent();
	}

    protected override void OnNavigatedTo(NavigatedToEventArgs args)
    {
		labelNavStack.Text = Shell.Current.Navigation.NavigationStack.Aggregate("Stack", (total, next) => $"{total} > {next?.Title ?? ""}");
	}

	private async void GoToPage1_1(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("page1_1");
	}

	private async void GoToPage1_2(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("page1/page1_2");
	}

	private async void GoToPage1_3(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("/page1_3");
	}
}