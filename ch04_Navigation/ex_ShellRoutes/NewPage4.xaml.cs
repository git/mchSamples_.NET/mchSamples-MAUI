namespace ex_ShellRoutes;

public partial class NewPage4 : ContentPage
{
	public NewPage4()
	{
		InitializeComponent();
	}

	private async void GoToNewPage5WithSlash(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("/newpage5");
	}

	private async void GoToNewPage5WithoutSlash(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("newpage5");
	}
}