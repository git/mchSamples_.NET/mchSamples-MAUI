﻿namespace ex_ShellRoutes;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();
		Routing.RegisterRoute("page1", typeof(Page1));
		Routing.RegisterRoute("//page1", typeof(Page1));
		Routing.RegisterRoute("page1/page1_1", typeof(Page1_1));
		Routing.RegisterRoute("page1/page1_2", typeof(Page1_2));
		Routing.RegisterRoute("page1/page1_2/page1_2_1", typeof(Page1_2_1));
		Routing.RegisterRoute("page1/page1_2/page1_2_2", typeof(Page1_2_2));
		Routing.RegisterRoute("page1/page1_2/page1_2_3", typeof(Page1_2_3));
		Routing.RegisterRoute("page1/page1_3", typeof(Page1_3));
		Routing.RegisterRoute("page2", typeof(Page2));
		Routing.RegisterRoute("//page1/page1_2/page1_2_2", typeof(Page1_2_2));
	}
}
