namespace ex_ShellRoutes;

public partial class Page1_2_1 : ContentPage
{
	public Page1_2_1()
	{
		InitializeComponent();
	}

    protected override void OnNavigatedTo(NavigatedToEventArgs args)
    {
		labelNavStack.Text = Shell.Current.Navigation.NavigationStack.Aggregate("Stack", (total, next) => $"{total} > {next?.Title ?? ""}");
	}
}