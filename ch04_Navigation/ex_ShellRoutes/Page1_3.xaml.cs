namespace ex_ShellRoutes;

public partial class Page1_3 : ContentPage
{
	public Page1_3()
	{
		InitializeComponent();
	}

    protected override void OnNavigatedTo(NavigatedToEventArgs args)
    {
		labelNavStack.Text = Shell.Current.Navigation.NavigationStack.Aggregate("Stack", (total, next) => $"{total} > {next?.Title ?? ""}");
	}
}