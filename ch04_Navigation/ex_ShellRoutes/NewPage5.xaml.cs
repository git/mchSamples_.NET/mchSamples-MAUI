namespace ex_ShellRoutes;

public partial class NewPage5 : ContentPage
{
	public NewPage5()
	{
		InitializeComponent();
	}

	private async void GoToNewPage4WithSlash(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("/newpage4");
	}

	private async void GoToNewPage4WithoutSlash(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync("newpage4");
	}
}