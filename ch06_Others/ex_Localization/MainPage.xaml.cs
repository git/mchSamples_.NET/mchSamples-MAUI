﻿using System.Globalization;
using ex_Localization.Resources.Localization;

namespace ex_Localization;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
	}

	void picker_SelectedIndexChanged(object source, EventArgs args)
	{
		string? chosenCultureString = picker.SelectedItem as string;

		CultureInfo chosenCulture = chosenCultureString switch 
		{
			"fr-FR" => new CultureInfo("fr-FR"),
			"en" => new CultureInfo("en"),
			"ca-ES" => new CultureInfo("ca-ES"),
			"es-ES" => new CultureInfo("es-ES"),
			"it" => new CultureInfo("it"),
			_ => new CultureInfo("en")
		};

		AppResources.Culture = chosenCulture;
	}
}

