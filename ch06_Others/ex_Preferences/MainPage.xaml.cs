﻿namespace ex_Preferences;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
	}

	private void ContentPage_Loaded(object sender, EventArgs e)
	{
		if(Preferences.Default.ContainsKey("one_setting"))
		{
			entry.Text = Preferences.Default.Get("one_setting", "nothing");	
		}
	}

	private void Entry_TextChanged(object sender, EventArgs e)
	{
		Preferences.Default.Set("one_setting", entry.Text);	
	}

	private void Button_Clicked(object sender, EventArgs e)
	{
		entry.Text = "";
		Preferences.Default.Clear();
	}


}

