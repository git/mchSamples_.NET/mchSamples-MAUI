using System.ComponentModel;
namespace ex_Localization2.Resources.Localization;

public class AppResourcesVM : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;

    void OnPropertyChanged(string propertyName)
        => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    public System.Globalization.CultureInfo Culture
    {
        get => AppResources.Culture;
        set
        {
            if(AppResources.Culture == value) return;
            
            AppResources.Culture = value;
            OnPropertyChanged("Culture");
            OnPropertyChanged("ButtonText");
            OnPropertyChanged("EntryPlaceholder");
            OnPropertyChanged("LabelText");
            OnPropertyChanged("PickerText");
        }
    }

    public string ButtonText => AppResources.ButtonText;

    public string EntryPlaceholder => AppResources.EntryPlaceholder;

    public string LabelText => AppResources.LabelText;

    public string PickerText => AppResources.PickerText;
}